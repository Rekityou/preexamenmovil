package com.example.preexamen;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class ReciboNominaActivity extends AppCompatActivity {
    private EditText etNombre, etHorasNormales, etHorasExtra;
    private TextView txtFolio, txtSubTotal, txtImpuesto, txtTotalAPagar, txtUsuario;
    private RadioGroup radiogroup;
    private Button btnCalcular, btnLimpiar, btnRegresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo_nomina);

        // Inicializa las vistas
        etNombre = findViewById(R.id.etNombre);
        etHorasNormales = findViewById(R.id.etHorasNormales);
        etHorasExtra = findViewById(R.id.etHorasExtra);
        txtFolio = findViewById(R.id.txtFolio);
        txtSubTotal = findViewById(R.id.txtSubTotal);
        txtImpuesto = findViewById(R.id.txtImpuesto);
        txtTotalAPagar = findViewById(R.id.txtTotalAPagar);
        txtUsuario = findViewById(R.id.txtUsuario);
        radiogroup = findViewById(R.id.radiogroup);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);

        // Recibe el nombre de usuario desde el Intent
        Bundle datos = getIntent().getExtras();
        String nombre = datos.getString("cliente");
        txtUsuario.setText(nombre);

        // Genera y muestra el número de recibo
        ReciboNomina recibo = new ReciboNomina();
        recibo.setNumRecibo(recibo.generarID());
        txtFolio.setText("Número de recibo: " + recibo.getNumRecibo());

        // Configura el botón de calcular
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validarCampos()) {
                    String nombre = etNombre.getText().toString();
                    int horasNormales = Integer.parseInt(etHorasNormales.getText().toString());
                    int horasExtra = Integer.parseInt(etHorasExtra.getText().toString());
                    int puesto = obtenerPuestoSeleccionado();
                    float porcentajeImpuesto = 16; // Fijo según las especificaciones

                    recibo.setNombre(nombre);
                    recibo.setHorasTrabajadasNormales(horasNormales);
                    recibo.setHorasTrabajadasExtra(horasExtra);
                    recibo.setPuesto(puesto);
                    recibo.setPorcentajeImpuesto(porcentajeImpuesto);

                    float subtotal = recibo.calcularSubtotal();
                    float impuesto = recibo.calcularImpuesto();
                    float total = recibo.calcularTotal();

                    txtSubTotal.setText("Subtotal: $" + String.format("%.2f", subtotal));
                    txtImpuesto.setText("Impuesto: $" + String.format("%.2f", impuesto));
                    txtTotalAPagar.setText("Total a pagar: $" + String.format("%.2f", total));
                } else {
                    Toast.makeText(ReciboNominaActivity.this, "Todos los campos son requeridos y deben ser válidos", Toast.LENGTH_SHORT).show();
                }
            }
        });

        // Configura el botón de limpiar
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiarCampos();
            }
        });

        // Configura el botón de regresar
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private boolean validarCampos() {
        return !etNombre.getText().toString().isEmpty() &&
                !etHorasNormales.getText().toString().isEmpty() &&
                !etHorasExtra.getText().toString().isEmpty();
    }

    private int obtenerPuestoSeleccionado() {
        int selectedId = radiogroup.getCheckedRadioButtonId();
        if (selectedId == R.id.auxiliar) {
            return 1;
        } else if (selectedId == R.id.albanil) {
            return 2;
        } else if (selectedId == R.id.ingObra) {
            return 3;
        }
        return 1; // Valor por defecto
    }

    private void limpiarCampos() {
        etNombre.setText("");
        etHorasNormales.setText("");
        etHorasExtra.setText("");
        radiogroup.check(R.id.auxiliar);
        txtSubTotal.setText("Subtotal:");
        txtImpuesto.setText("Impuesto:");
        txtTotalAPagar.setText("Total a pagar:");
    }
}

//Se realizaron pruebas y el programa funciona
//Se finalizo el programa