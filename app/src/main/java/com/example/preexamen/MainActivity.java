package com.example.preexamen;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private EditText txtUsuario;
    private Button btnIngresar, btnRegresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Inicializa las vistas
        txtUsuario = findViewById(R.id.txtUsuario);
        btnIngresar = findViewById(R.id.btnIngresar);
        btnRegresar = findViewById(R.id.btnRegresar);

        // Configura el botón de ingresar
        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String usuario = txtUsuario.getText().toString();
                if (usuario.isEmpty()) {
                    Toast.makeText(MainActivity.this, "Por favor, ingrese un usuario", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(MainActivity.this, ReciboNominaActivity.class);
                    intent.putExtra("cliente", usuario);
                    startActivity(intent);
                }
            }
        });

        // Configura el botón de regresar
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Acción al presionar el botón de regresar (por ejemplo, finalizar la actividad)
                finish();
            }
        });
    }
}
