package com.example.preexamen;

import java.util.Random;

public class ReciboNomina {
    private int numRecibo;
    private String nombre;
    private int horasTrabajadasNormales;
    private int horasTrabajadasExtra;
    private int puesto;
    private float porcentajeImpuesto;

    // Constructor sin argumentos
    public ReciboNomina() {
    }
    public ReciboNomina(int numRecibo, String nombre, int horasTrabajadasNormales, int horasTrabajadasExtra, int puesto, float porcentajeImpuesto) {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.horasTrabajadasNormales = horasTrabajadasNormales;
        this.horasTrabajadasExtra = horasTrabajadasExtra;
        this.puesto = puesto;
        this.porcentajeImpuesto = porcentajeImpuesto;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getHorasTrabajadasNormales() {
        return horasTrabajadasNormales;
    }

    public void setHorasTrabajadasNormales(int horasTrabajadasNormales) {
        this.horasTrabajadasNormales = horasTrabajadasNormales;
    }

    public int getHorasTrabajadasExtra() {
        return horasTrabajadasExtra;
    }

    public void setHorasTrabajadasExtra(int horasTrabajadasExtra) {
        this.horasTrabajadasExtra = horasTrabajadasExtra;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public float getPorcentajeImpuesto() {
        return porcentajeImpuesto;
    }

    public void setPorcentajeImpuesto(float porcentajeImpuesto) {
        this.porcentajeImpuesto = porcentajeImpuesto;
    }

    public float calcularPagoBase() {
        float pagoBase = 200;
        switch (puesto) {
            case 1:
                pagoBase += pagoBase * 0.2;
                break;
            case 2:
                pagoBase += pagoBase * 0.5;
                break;
            case 3:
                pagoBase += pagoBase * 1.0;
                break;
        }
        return pagoBase;
    }

    public float calcularSubtotal() {
        float pagoBase = calcularPagoBase();
        return (pagoBase * horasTrabajadasNormales) + (pagoBase * 2 * horasTrabajadasExtra);
    }

    public float calcularImpuesto() {
        return calcularSubtotal() * (porcentajeImpuesto / 100);
    }

    public float calcularTotal() {
        return calcularSubtotal() - calcularImpuesto();
    }

    public int generarID(){
        Random r = new Random();
        return r.nextInt(1000);
    }
}